use std::fmt::Display;

fn main() -> () {
    let triple = Triple { a: 3, b: 4 };
    for a in 1.. {
        for b in a.. {
            let t = Triple { a, b };
            if is_perfect_square(t.pythag()) {
                let c = t.pythag() as f64;
                let c = c.sqrt();
                let c = c as i64;
                let res = t.a + t.b + c;
                if res == 1000 {
                    println!("{} {} {} => {}", t.a, t.b, c, t.a * t.b * c);
                }
            }
            if t.pythag() > 1000000 {
                break;
            }
        }
        if a * a > 1000000 {
            break;
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Params {
    A,
    B,
}

#[derive(Debug, Clone, Copy)]
pub struct Triple {
    pub a: i64,
    pub b: i64,
}

impl Triple {
    fn pythag(&self) -> i64 {
        self.a * self.a + self.b * self.b
    }
}

impl Display for Triple {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let res = self.a.pow(2) + self.b.pow(2);
        write!(f, "{}^2 + {}^2 = {}", self.a, self.b, res)
    }
}

#[derive(Debug)]
pub struct TripleGenerator {
    last_incremented: Params,
    previous_triple: Triple,
}

impl Iterator for TripleGenerator {
    type Item = Triple;

    fn next(&mut self) -> Option<Self::Item> {
        let a = if self.last_incremented == Params::A {
            self.previous_triple.a + 1
        } else {
            self.previous_triple.a
        };
        let b = if self.last_incremented == Params::B {
            self.previous_triple.b + 1
        } else {
            self.previous_triple.b
        };
        let inc = match self.last_incremented {
            Params::A => Params::B,
            Params::B => Params::A,
        };
        let triple = Triple { a, b };
        self.last_incremented = inc;
        self.previous_triple = triple;
        Some(triple)
    }
}

fn is_perfect_square(num: i64) -> bool {
    let sqnum = (num as f64).sqrt().ceil();
    let sqnum = sqnum as i64;
    for x in 1..sqnum + 1 {
        if x * x == num {
            return true;
        }
    }
    false
}
